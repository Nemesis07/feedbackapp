package com.feedbacksystem.Students;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.feedbacksystem.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.shitij.goyal.slidebutton.SwipeButton;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private static final int  REQUEST_ACCESS_CAMERA = 111;
    static int count = 0;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private  FirebaseAuth.AuthStateListener mAuthListener;
    private String TAG="";
    private String pass;
    private String mail;
    private String name;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressDialog=new ProgressDialog(this);
        firebaseAuth=FirebaseAuth.getInstance();
        SwipeButton swipeButton = (SwipeButton) findViewById(R.id.slide);
        swipeButton.addOnSwipeCallback(new SwipeButton.Swipe() {
                                           @Override
                                           public void onButtonPress() {
                                               //  Toast.makeText(MainActivity.this, "Pressed!", Toast.LENGTH_SHORT).show();
                                           }

                                           @Override
                                           public void onSwipeCancel() {

                                           }

                                           @Override
                                           public void onSwipeConfirm() {
                                               //  Toast.makeText(MainActivity.this, "confirmed!", Toast.LENGTH_LONG).show();

                                               new IntentIntegrator(MainActivity.this).initiateScan();  //IntentIntegrator from zxing allows us to use the barcode scanning and decoding without going through the source code


                                               boolean hasPermissionCamera = (ContextCompat.checkSelfPermission(getApplicationContext(),
                                                       Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
                                               if (!hasPermissionCamera) {
                                                   ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_ACCESS_CAMERA);   //If the app does not have the camera permission it explicitly asks the user to give the permission

                                               }
                                           }
                                       });
            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {

                        count++;
                        System.out.println(count);
                        if(count%2!=0)
                        {
                            final String email = user.getEmail();
                            System.out.println("SignedIn");
                            Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                            progressDialog.setMessage("Logging in User");
                            progressDialog.show();
                            progressDialog.setCancelable(false);
                            progressDialog.setCanceledOnTouchOutside(false);

                            FirebaseDatabase database = FirebaseDatabase.getInstance();

                            final DatabaseReference myRef = database.getReference("User");

                            //System.out.println(myRef.child(ui).getKey());
                            myRef.orderByChild("email").equalTo(email).limitToFirst(1).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                                        UID = childSnapshot.getKey();

                                        mail = (String) childSnapshot.child("email").getValue();
                                        pass = (String) childSnapshot.child("password").getValue();
                                        name = (String) childSnapshot.child("name").getValue();
                                    }
                                    System.out.println("Password1 " + pass);
                                    System.out.println("Email1 " + email);
                                    System.out.println("UID1 " + UID);
                                    System.out.println("Finaluid " + UID);
                                    progressDialog.cancel();
                                    int year = Calendar.getInstance().get(Calendar.YEAR);
                                    int curryear = year - Integer.parseInt(UID.substring(0, 4));
                                    int depno = Integer.parseInt(UID.substring(5, 6));
                                    System.out.println(depno);

                                    Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                                    intent.putExtra("uid", UID);
                                    intent.putExtra("name", name);
                                    intent.putExtra("curryear", curryear);
                                    if (depno == 1)
                                        intent.putExtra("depno", "ETRX");
                                    else if (depno == 2)
                                        intent.putExtra("depno", "EXTC");
                                    else if (depno == 3)
                                        intent.putExtra("depno", "COMPS");
                                    else if (depno == 4)
                                        intent.putExtra("depno", "IT");
                                    //  FLAG="1";
                                    finish();
                                    startActivity(intent);
                                }


                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }

                            });
                        }

                    }

                    else {
                        // User is signed out

                        Log.d(TAG, "onAuthStateChanged:signed_out");
                        System.out.println("SignedOut");
                        // FLAG="0";
                    }

                }};





    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onPause(){
        super.onPause();
        firebaseAuth.removeAuthStateListener(mAuthListener);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            firebaseAuth.removeAuthStateListener(mAuthListener);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultcode, Intent data) {   //This function used to handle the result
       final IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultcode, data);
        if (result != null) {
            if (result.getContents() == null) {

                // Log.d("MainActivity","Cancelled Scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                // Log.d("MainActivity","Scanned");
               // Toast.makeText(this, "Scanned:" + result.getContents(), Toast.LENGTH_LONG).show();
                progressDialog.setMessage("Logging in User");
                progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("User");
                System.out.println(myRef.child(result.getContents()).getKey());
                myRef.child(result.getContents()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //String acctname = (String)dataSnapshot.child("Name").getValue();
                        String email = (String) dataSnapshot.child("email").getValue();
                        String password=(String)dataSnapshot.child("password").getValue();
                        final String name=(String)dataSnapshot.child("name").getValue();
                        System.out.println(password);
                        System.out.println(email);
                        if(email!=null && password!=null) {
                            firebaseAuth.signInWithEmailAndPassword(email, password)
                                    .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            // If sign in fails, display a message to the user. If sign in succeeds
                                            // the auth state listener will be notified and logic to handle the
                                            // signed in user can be handled in the listener.
                                            progressDialog.cancel();
                                            if (task.isSuccessful()) {
                                                Toast.makeText(MainActivity.this, "Login Successful", Toast.LENGTH_LONG).show();



                                            }
                                            if (!task.isSuccessful()) {
                                                // there was an error

                                                Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_LONG).show();

                                            }
                                        }
                                    });

                        }
                        else{
                            progressDialog.cancel();
                            Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("Cancelled");
                    }
                });
            }

        } else {
            super.onActivityResult(requestCode, resultcode, data);
        }
    }
}