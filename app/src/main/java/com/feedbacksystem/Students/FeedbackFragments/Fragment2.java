package com.feedbacksystem.Students.FeedbackFragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.feedbacksystem.R;

public class Fragment2 extends android.support.v4.app.Fragment {
    private RadioGroup radiogroup;
    private RadioButton r1;
    private RadioButton r2;
    private RadioButton r3;
    private View rootView;
    private EditText CourseExit;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup container, @Nullable Bundle savedinstances)
    {
        String Activity = getArguments().getString("Activity");
        if (Activity.equals("Feedback")) {
            rootView = layoutInflater.inflate(R.layout.tab2, container, false);
            radiogroup = (RadioGroup) rootView.findViewById(R.id.radiogroup2);
            r1 = (RadioButton) rootView.findViewById(R.id.radioButton);
            r2 = (RadioButton) rootView.findViewById(R.id.radioButton2);
            r3 = (RadioButton) rootView.findViewById(R.id.radioButton3);
            preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
             editor = preferences.edit();
//System.out.println("Fragment2");
            if ((preferences.getString("Result2", null) != null)) {
                editor.remove("Result2");
                editor.apply();
            }
            r1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // int selectid=radiogroup.getCheckedRadioButtonId();
                    // System.out.println(selectid);
                    // RadioButton radioButton=(RadioButton)rootView.findViewById(selectid);
                    String rb = r1.getText().toString();
                  //  System.out.println(rb);

                    editor.putString("Result2", "5");
                    editor.apply();
                }
            });
            r2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // int selectid=radiogroup.getCheckedRadioButtonId();
                    // System.out.println(selectid);
                    // RadioButton radioButton=(RadioButton)rootView.findViewById(selectid);
                    String rb = r2.getText().toString();
                   // System.out.println(rb);

                    editor.putString("Result2", "3");
                    editor.apply();
                }
            });
            r3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // int selectid=radiogroup.getCheckedRadioButtonId();
                    // System.out.println(selectid);
                    // RadioButton radioButton=(RadioButton)rootView.findViewById(selectid);
                    String rb = r3.getText().toString();
                   // System.out.println(rb);

                    editor.putString("Result2", "1");
                    editor.apply();
                }
            });
        }
        else if(Activity.equals("Course"))
        {
            rootView = layoutInflater.inflate(R.layout.course2, container, false);
            CourseExit=(EditText)rootView.findViewById(R.id.courseExit2);
           // CourseExit.setText("");
            String response=CourseExit.getText().toString();
             preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
             editor = preferences.edit();
        //    System.out.println("Fragment2");
            if ((preferences.getString("Response2", null) != null)) {
                editor.remove("Response2");
                editor.apply();
            }
           // System.out.println(response);
            CourseExit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                   /* System.out.println(response);
                    editor.putString("Response1",response);
                    editor.apply();*/
               }

                @Override
                public void afterTextChanged(Editable s) {
                   // System.out.println(response);
                    editor.putString("Response2",s.toString());
                    editor.apply();
                }
            });
        }
        return rootView;
    }
}

