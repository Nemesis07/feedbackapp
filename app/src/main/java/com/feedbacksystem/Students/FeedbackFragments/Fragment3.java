package com.feedbacksystem.Students.FeedbackFragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;

import com.feedbacksystem.R;

public class Fragment3 extends android.support.v4.app.Fragment {

    private RadioButton r1;
    private RadioButton r2;
    private RadioButton r3;
    private View rootView;
    private EditText CourseExit;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup container, @Nullable Bundle savedinstances)
    {
        String Activity = getArguments().getString("Activity");
        if (Activity.equals("Feedback")) {
            rootView = layoutInflater.inflate(R.layout.tab3, container, false);
            r1 = (RadioButton) rootView.findViewById(R.id.radioButton);
            r2 = (RadioButton) rootView.findViewById(R.id.radioButton2);
            r3 = (RadioButton) rootView.findViewById(R.id.radioButton3);
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            final SharedPreferences.Editor editor = preferences.edit();
            //System.out.println("Fragment3");
            if ((preferences.getString("Result3", null) != null)) {
                editor.remove("Result3");
                editor.apply();
            }
            r1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // int selectid=radiogroup.getCheckedRadioButtonId();
                    // System.out.println(selectid);
                    // RadioButton radioButton=(RadioButton)rootView.findViewById(selectid);
                    String rb = r1.getText().toString();
                    //System.out.println(rb);

                    editor.putString("Result3", "5");
                    editor.apply();
                }
            });
            r2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // int selectid=radiogroup.getCheckedRadioButtonId();
                    // System.out.println(selectid);
                    // RadioButton radioButton=(RadioButton)rootView.findViewById(selectid);
                    String rb = r2.getText().toString();
                  //  System.out.println(rb);

                    editor.putString("Result3", "3");
                    editor.apply();
                }
            });
            r3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // int selectid=radiogroup.getCheckedRadioButtonId();
                    // System.out.println(selectid);
                    // RadioButton radioButton=(RadioButton)rootView.findViewById(selectid);
                    String rb = r3.getText().toString();
                //    System.out.println(rb);

                    editor.putString("Result3", "1");
                    editor.apply();
                }
            });
        }else if(Activity.equals("Course"))
        {
            rootView = layoutInflater.inflate(R.layout.course3, container, false);
            CourseExit=(EditText)rootView.findViewById(R.id.courseExit3);
            CourseExit.setText("");
            String response=CourseExit.getText().toString();
            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            final SharedPreferences.Editor editor = preferences.edit();
            //System.out.println("Fragment1");
            if ((preferences.getString("Response3", null) != null)) {
                editor.remove("Response3");
                editor.apply();
            }

            CourseExit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                   /* System.out.println(response);
                    editor.putString("Response1",response);
                    editor.apply();*/
                }

                @Override
                public void afterTextChanged(Editable s) {

                    editor.putString("Response3",s.toString());
                    editor.apply();
                }
            });
        }





        return rootView;
    }
}

