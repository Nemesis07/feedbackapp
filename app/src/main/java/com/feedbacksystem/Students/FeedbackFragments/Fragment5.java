package com.feedbacksystem.Students.FeedbackFragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.feedbacksystem.R;
import com.feedbacksystem.Students.CourseSubjectActivity;
import com.feedbacksystem.Students.SubjectActivity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Fragment5 extends android.support.v4.app.Fragment {

    private RadioButton r1;
    private RadioButton r2;
    private RadioButton r3;
    private Button submit;
    private FirebaseDatabase database;
    private DatabaseReference myref;
    private View rootView;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private SharedPreferences pref;
    private SharedPreferences.Editor edit;
    private EditText CourseExit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup container, @Nullable Bundle savedinstances) {
         String Activity = getArguments().getString("Activity");
        assert Activity != null;
        if (Activity.equals("Feedback")) {
        rootView = layoutInflater.inflate(R.layout.tab5, container, false);
        r1 = (RadioButton) rootView.findViewById(R.id.radioButton);
        r2 = (RadioButton) rootView.findViewById(R.id.radioButton2);
        r3 = (RadioButton) rootView.findViewById(R.id.radioButton3);
        submit = (Button) rootView.findViewById(R.id.submit);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = preferences.edit();
        //System.out.println("Fragment5");
        if ((preferences.getString("Result5", null) != null)) {
            editor.remove("Result5");
            editor.apply();
        }
        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // int selectid=radiogroup.getCheckedRadioButtonId();
                // System.out.println(selectid);
                // RadioButton radioButton=(RadioButton)rootView.findViewById(selectid);
                String rb = r1.getText().toString();
                //  System.out.println(rb);

                editor.putString("Result5", "5");
                editor.apply();
            }
        });
        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // int selectid=radiogroup.getCheckedRadioButtonId();
                // System.out.println(selectid);
                // RadioButton radioButton=(RadioButton)rootView.findViewById(selectid);
                String rb = r2.getText().toString();
                //    System.out.println(rb);

                editor.putString("Result5", "3");
                editor.apply();
            }
        });
        r3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // int selectid=radiogroup.getCheckedRadioButtonId();
                // System.out.println(selectid);
                // RadioButton radioButton=(RadioButton)rootView.findViewById(selectid);
                String rb = r3.getText().toString();
                //      System.out.println(rb);

                editor.putString("Result5", "1");
                editor.apply();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check();
            }


        });
        } else if (Activity.equals("Course")) {
            rootView = layoutInflater.inflate(R.layout.course5, container, false);
            CourseExit = (EditText) rootView.findViewById(R.id.courseExit5);
            CourseExit.setText("");
            submit = (Button) rootView.findViewById(R.id.submit);
            String response = CourseExit.getText().toString();
             preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            editor = preferences.edit();
            String res1 = preferences.getString("Response5", null);
            //System.out.println(res1);
            //System.out.println("Fragment1");
            if ((preferences.getString("Response5", null) != null)) {
                editor.remove("Response5");
                editor.apply();
            }

            CourseExit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                   /* System.out.println(response);
                    editor.putString("Response1",response);
                    editor.apply();*/
  }

                @Override
                public void afterTextChanged(Editable s) {

                    editor.putString("Response5",s.toString());
                    editor.apply();
                }
            });
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    coursecheck();
                }
            });
        }


        return rootView;
}

    public void check() {
        String res1 = preferences.getString("Result1", null);

        String res2 = preferences.getString("Result2", null);
        String res3 = preferences.getString("Result3", null);
        String res4 = preferences.getString("Result4", null);
        String res5 = preferences.getString("Result5", null);
        if (res1 != null && res2 != null && res3 != null && res4 != null && res5 != null) {
            database = FirebaseDatabase.getInstance();
            myref = database.getReference("User");
            String uid = getArguments().getString("uid");
            String Subject = getArguments().getString("Subject");
            //System.out.println(uid);
            //System.out.println(res1);
            //System.out.println(res2);
            //System.out.println(res3);
            //System.out.println(res4);
            //System.out.println(res5);
            // System.out.println(getArguments().getString("curryear"));
            //System.out.println(getArguments().getString("dept"));
            System.out.println(Subject);
            //System.out.println("Fragment check");
            myref.child(uid).child("Feedback").child(Subject).child("Q1").setValue(res1);
            myref.child(uid).child("Feedback").child(Subject).child("Q2").setValue(res2);
            myref.child(uid).child("Feedback").child(Subject).child("Q3").setValue(res3);
            myref.child(uid).child("Feedback").child(Subject).child("Q4").setValue(res4);
            myref.child(uid).child("Feedback").child(Subject).child("Q5").setValue(res5);
            myref.child(uid).child("Feedback").child(Subject).child("filled").setValue(true);
            Toast.makeText(getContext(), "Thank you", Toast.LENGTH_SHORT).show();
            editor.putString("FLAG","0");
            editor.putString("Subject", Subject);
            editor.apply();
            Intent intent = new Intent(getActivity(), SubjectActivity.class);
            intent.putExtra("uid", uid);
            intent.putExtra("curryear", Integer.parseInt(getArguments().getString("curryear")));
            intent.putExtra("dept", getArguments().getString("dept"));
            intent.putExtra("Subject", getArguments().getString("Subject"));
            intent.putExtra("Activity",getArguments().getString("Activity"));
            getActivity().setResult(Activity.RESULT_OK, intent);
            // System.out.println(getActivity());
            // getActivity().startActivity(intent);
            getActivity().finish();
        }
        else
            Toast.makeText(getContext(), "Please select all the buttons", Toast.LENGTH_SHORT).show();

    }


    public void coursecheck() {
        String res1 = preferences.getString("Response1", null);
        String res2 = preferences.getString("Response2", null);
        String res3 = preferences.getString("Response3", null);
        String res4 = preferences.getString("Response4", null);
        String res5 = preferences.getString("Response5", null);
        if (res1 != null && res2 != null && res3 != null && res4 != null && res5 != null) {
            database = FirebaseDatabase.getInstance();
            myref = database.getReference("User");
            String uid = getArguments().getString("uid");
            String Subject = getArguments().getString("Courses");
            //System.out.println(uid);
            //System.out.println(res1);
            //System.out.println(res2);
            //System.out.println(res3);
            //System.out.println(res4);
            //System.out.println(res5);
            // System.out.println(getArguments().getString("curryear"));
            //System.out.println(getArguments().getString("dept"));
            System.out.println(Subject);
            //System.out.println("Fragment check");
            myref.child(uid).child("Course").child(Subject).child("Q1").setValue(res1);
            myref.child(uid).child("Course").child(Subject).child("Q2").setValue(res2);
            myref.child(uid).child("Course").child(Subject).child("Q3").setValue(res3);
            myref.child(uid).child("Course").child(Subject).child("Q4").setValue(res4);
            myref.child(uid).child("Course").child(Subject).child("Q5").setValue(res5);
            myref.child(uid).child("Course").child(Subject).child("filled").setValue(true);
            Toast.makeText(getContext(), "Thank you", Toast.LENGTH_SHORT).show();
           // editor.putString("FLAG","1");
            editor.putString("Courses", Subject);
            editor.apply();
            Intent intent = new Intent(getActivity(),CourseSubjectActivity.class);
            intent.putExtra("uid", uid);
            intent.putExtra("curryear", Integer.parseInt(getArguments().getString("curryear")));
            intent.putExtra("dept", getArguments().getString("dept"));
            intent.putExtra("Subject", getArguments().getString("Courses"));
            intent.putExtra("Activity",getArguments().getString("Activity"));
            getActivity().setResult(Activity.RESULT_OK, intent);
            // System.out.println(getActivity());
            // getActivity().startActivity(intent);
            getActivity().finish();
        }
        else
            Toast.makeText(getContext(), "Please fill all the fields", Toast.LENGTH_SHORT).show();

    }
}

