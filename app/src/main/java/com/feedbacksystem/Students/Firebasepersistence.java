package com.feedbacksystem.Students;

import com.google.firebase.database.FirebaseDatabase;


public class Firebasepersistence extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
    /* Enable disk persistence  */
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

    }
}
