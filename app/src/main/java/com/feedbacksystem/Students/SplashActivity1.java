package com.feedbacksystem.Students;



import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.feedbacksystem.R;


public class SplashActivity1 extends AppCompatActivity{

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        //setTheme(R.style.splashScreenTheme);

        super.onCreate(icicle);
        setContentView(R.layout.splash1);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        /* Duration of wait */
        int SPLASH_DISPLAY_LENGTH = 1000;
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashActivity1.this,LoginActivity.class);
                SplashActivity1.this.startActivity(mainIntent);
                SplashActivity1.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
