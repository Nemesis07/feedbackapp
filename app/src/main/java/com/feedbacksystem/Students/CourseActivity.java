package com.feedbacksystem.Students;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.feedbacksystem.R;
import com.feedbacksystem.Students.FeedbackFragments.Fragment1;
import com.feedbacksystem.Students.FeedbackFragments.Fragment2;
import com.feedbacksystem.Students.FeedbackFragments.Fragment3;
import com.feedbacksystem.Students.FeedbackFragments.Fragment4;
import com.feedbacksystem.Students.FeedbackFragments.Fragment5;

public class CourseActivity extends AppCompatActivity {
    private Bundle bundle;
    private SectionsPagerAdapter mSectionsPagerAdapter;


    private CustomPageViewer mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Subject "+getIntent().getStringExtra("Courses"));
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (CustomPageViewer) findViewById(R.id.container);
        mViewPager.setPagingEnabled(false);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor=preferences.edit().clear();
        bundle=new Bundle();
        bundle.putString("uid",getIntent().getStringExtra("uid"));
        bundle.putString("Courses",getIntent().getStringExtra("Courses"));
        bundle.putString("dept",getIntent().getStringExtra("dept"));
        bundle.putString("curryear",getIntent().getStringExtra("curryear"));
        bundle.putString("Activity",getIntent().getStringExtra("Activity"));

        //     String result1 = preferences.getString("Result1","");
        //   String result2 = preferences.getString("Result1","");
        // System.out.println(result1);
        System.out.println("Feedbackactivity");


    }





    public void jumptonext2(View view) {
        mViewPager.setCurrentItem(1,true);
    }

    public void jumptonext3(View view) {
        mViewPager.setCurrentItem(2,true);
    }

    public void jumptoprev1(View view) {
        mViewPager.setCurrentItem(0,true);
    }

    public void jumptonext4(View view) {
        mViewPager.setCurrentItem(3,true);
    }

    public void jumptoprev2(View view) {
        mViewPager.setCurrentItem(1,true);
    }

    public void jumptoprev3(View view) {
        mViewPager.setCurrentItem(2,true);
    }

    public void jumptonext5(View view) {
        mViewPager.setCurrentItem(4,true);
    }

    public void jumptoprev4(View view) {
        mViewPager.setCurrentItem(3,true);
    }

  /* public void Submit(View view) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null && fragment.isVisible()) {
            if (fragment instanceof Fragment5) {
                ((Fragment5) fragment).check();
            }
        }
    }*/

    /**
     * A placeholder fragment containing a simple view.
     */
    // public static class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    //   private static final String ARG_SECTION_NUMBER = "section_number";

    // public PlaceholderFragment() {
    //   }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
       /* public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }*/

       /* @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.tab1, container, false);
            //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
       } }*/


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position){
                case 0:
                    Fragment1 fragment1=new Fragment1();
                    fragment1.setArguments(bundle);
                    return fragment1;
                case 1:
                    Fragment2 fragment2=new Fragment2();
                    fragment2.setArguments(bundle);
                    return fragment2;
                case 2:
                    Fragment3 fragment3=new Fragment3();
                    fragment3.setArguments(bundle);
                    return fragment3;
                case 3:
                    Fragment4 fragment4=new Fragment4();
                    fragment4.setArguments(bundle);
                    return fragment4;
                case 4:
                    Fragment5 fragment5=new Fragment5();
                    fragment5.setArguments(bundle);
                    return fragment5;


            }
            return  null;
        }
   /* public int getItem(int position){
        return mViewPager.getCurrentItem() + 1;
    }*/

        @Override
        public int getCount() {
            // Show 5 total pages.
            return 5;
        }

      /*  @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 3";
                case 4:
                    return "SECTION 3";
            }
            return null;
        }*/
    }
}
