package com.feedbacksystem.Students;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.feedbacksystem.R;
import com.google.firebase.auth.FirebaseAuth;


public class DashboardActivity extends AppCompatActivity {
    private TextView test;
    private Button logout;
    private TextView year;
    private LinearLayout Feedback;
    private LinearLayout Course;
    private String FLAG;

    @Override
    protected void onCreate(Bundle savedinstances)
    {
        super.onCreate(savedinstances);
        setContentView(R.layout.activity_dashboard);
        test=(TextView)findViewById(R.id.test);
year=(TextView)findViewById(R.id.year);
        Feedback=(LinearLayout)findViewById(R.id.Feedback);
        Course=(LinearLayout)findViewById(R.id.Course);
        final int Curryear=getIntent().getIntExtra("curryear",0);
        final String dept=getIntent().getStringExtra("depno");
        final String uid=getIntent().getStringExtra("uid");
       /* SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor=preferences.edit();
        FLAG=preferences.getString("FLAG",null);
        System.out.println(FLAG);*/
        System.out.println("Dashboard");
        if(getIntent().getIntExtra("curryear",0)==1)
        {
            year.setText("FE "+dept);
        }
        else  if(getIntent().getIntExtra("curryear",0)==2)
        {
            year.setText("SE "+dept);
        }
        else if(getIntent().getIntExtra("curryear",0)==3)
        {
            year.setText("TE "+dept);
        }
        else  if(getIntent().getIntExtra("curryear",0)==4)
        {
            year.setText("BE "+dept);
        }
        test.setText("Welcome "+getIntent().getStringExtra("name"));
        Feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DashboardActivity.this,SubjectActivity.class);
                intent.putExtra("dept",dept);
                intent.putExtra("curryear",Curryear);
                intent.putExtra("uid",uid);
                intent.putExtra("Activity","Feedback");
           //     FLAG="0";
                //editor.remove("FLAG");
           //     editor.putString("FLAG",FLAG);
             //   editor.apply();

                startActivity(intent);
            }
        });
        Course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DashboardActivity.this,CourseSubjectActivity.class);
                intent.putExtra("dept",dept);
                intent.putExtra("curryear",Curryear);
                intent.putExtra("uid",uid);
                intent.putExtra("Activity","Course");

                startActivity(intent);
            }
        });
        logout=(Button)findViewById(R.id.Logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Userlogout();
            }
        });
    }



    public void Userlogout(){
        FirebaseAuth.getInstance().signOut();

        Intent intent=new Intent(this,LoginButtonActivity.class);
        startActivity(intent);
        finish();
    }




@Override
    public void onBackPressed(){
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage("Are you sure you want to Logout?")
            .setCancelable(true)
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            })
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {

                    FirebaseAuth.getInstance().signOut();
                    finish();
                    Intent intent=new Intent(DashboardActivity.this,LoginButtonActivity.class);
                    startActivity(intent);
                    DashboardActivity.this.finish();
                }
            });
    AlertDialog alert = builder.create();
    alert.show();
}
}
