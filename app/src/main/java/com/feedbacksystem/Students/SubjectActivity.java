package com.feedbacksystem.Students;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.feedbacksystem.R;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SubjectActivity extends AppCompatActivity {
    private static Set<String> subjects = new HashSet<>();
    private static Set<String> coursesub = new HashSet<>();
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference myref, myref2;
    private CardView c1,c2,c3,c4,c5,c6;
    private TextView st1,st2,st3,st4,st5,st6;
    private String s1,s2,s3,s4,s5,s6,Subject,Subject2;

    @Override
    public void onCreate(Bundle savedInstances) {

        super.onCreate(savedInstances);
        setContentView(R.layout.activity_subjects);
        c1 = (CardView) findViewById(R.id.card_view_top);
        c2 = (CardView) findViewById(R.id.card_view_middle1);
        c3 = (CardView) findViewById(R.id.card_view_middle2);
        c4 = (CardView) findViewById(R.id.card_view_middle3);
        c5 = (CardView) findViewById(R.id.card_view_middle4);
        c6 = (CardView) findViewById(R.id.card_view_bottom);
        st1 = (TextView) findViewById(R.id.s1);
        st2 = (TextView) findViewById(R.id.s2);
        st3 = (TextView) findViewById(R.id.s3);
        st4 = (TextView) findViewById(R.id.s4);
        st5 = (TextView) findViewById(R.id.s5);
        st6 = (TextView) findViewById(R.id.s6);


        final String uid = getIntent().getStringExtra("uid");
        final String dept = getIntent().getStringExtra("dept");
        final String curryear = Integer.toString(getIntent().getIntExtra("curryear", 0));
        final String Activity=getIntent().getStringExtra("Activity");
       final NiftyDialogBuilder dialogBuilder=NiftyDialogBuilder.getInstance(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        myref = FirebaseDatabase.getInstance().getReference("Subjects");
        myref2 = FirebaseDatabase.getInstance().getReference("User");
        myref.child(curryear).child(dept).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                s1 = dataSnapshot.child("S1").getValue().toString();
                s2 = dataSnapshot.child("S2").getValue().toString();
                s3 = dataSnapshot.child("S3").getValue().toString();
                s4 = dataSnapshot.child("S4").getValue().toString();
                s5 = dataSnapshot.child("S5").getValue().toString();
                s6 = dataSnapshot.child("S6").getValue().toString();

                st1.setText(s1);
                st2.setText(s2);
                st3.setText(s3);
                st4.setText(s4);
                st5.setText(s5);
                st6.setText(s6);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        System.out.println("SubjectActivity");

            myref2.child(uid).child("Feedback").orderByChild("filled").equalTo(true).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        if(!(subjects.contains(childSnapshot.getKey())))
                        subjects.add(childSnapshot.getKey());
                    }
                }


                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            myref2.child(uid).child("Feedback").orderByChild("filled").equalTo(false).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        subjects.remove(childSnapshot.getKey());

                    }
                }


                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

                Iterator<String> itr = subjects.iterator();
                while (itr.hasNext()) {
                    System.out.print(itr.next() + " ");
                }
                System.out.println();


                c1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!(subjects.contains("SE"))) {
                            System.out.println("Clicked SE");
                            Intent intent = new Intent(SubjectActivity.this, FeedbackActivity.class);
                            intent.putExtra("uid", uid);
                            intent.putExtra("dept", dept);
                            intent.putExtra("curryear", curryear);
                            intent.putExtra("Subject",s1);
                            intent.putExtra("Activity", Activity);
                            startActivityForResult(intent, 1);
                        } else if (subjects.contains("SE")) {//if(substatus.get("SE").equals("true"))
                            dialogBuilder
                                    .withTitle("Feedback System")
                                    .withMessage("You have already filled the form.")
                                    .withDialogColor("#1976D2")
                                    .withButton1Text("OK")
                                    .withDuration(700)
                                    .withEffect(Effectstype.RotateBottom)
                                    .setButton1Click(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialogBuilder.cancel();
                                        }
                                    })
                                    .show();
                            //Toast.makeText(SubjectActivity.this, "You have already filled the form", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


                c2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!(subjects.contains("MCC"))) {

                            Intent intent = new Intent(SubjectActivity.this, FeedbackActivity.class);
                            intent.putExtra("uid", uid);
                            intent.putExtra("dept", dept);
                            intent.putExtra("curryear", curryear);
                            intent.putExtra("Subject", s2);
                            intent.putExtra("Activity", Activity);
                            startActivityForResult(intent, 2);
                        } else if (subjects.contains("MCC")) {
                            dialogBuilder
                                    .withTitle("Feedback System")
                                    .withMessage("You have already filled the form.")
                                    .withDialogColor("#1976D2")
                                    .withButton1Text("OK")
                                    .withDuration(700)
                                    .withEffect(Effectstype.RotateBottom)
                                    .setButton1Click(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialogBuilder.cancel();
                                        }
                                    })
                                    .show();
                           // Toast.makeText(SubjectActivity.this, "You have already filled the form", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


                c3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!(subjects.contains("NPL"))) {

                            Intent intent = new Intent(SubjectActivity.this, FeedbackActivity.class);
                            intent.putExtra("uid", uid);
                            intent.putExtra("dept", dept);
                            intent.putExtra("curryear", curryear);
                            intent.putExtra("Subject", s3);
                            intent.putExtra("Activity", Activity);
                            startActivityForResult(intent, 3);
                        } else if (subjects.contains("NPL"))
                        {
                            dialogBuilder
                                    .withTitle("Feedback System")
                                    .withMessage("You have already filled the form.")
                                    .withDialogColor("#1976D2")
                                    .withButton1Text("OK")
                                    .withDuration(700)
                                    .withEffect(Effectstype.RotateBottom)
                                    .setButton1Click(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialogBuilder.cancel();
                                        }
                                    })
                                    .show();
                        }
                            //Toast.makeText(SubjectActivity.this, "You have already filled the form", Toast.LENGTH_SHORT).show();
                    }
                });


                c4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!(subjects.contains("DDB"))) {

                            Intent intent = new Intent(SubjectActivity.this, FeedbackActivity.class);
                            intent.putExtra("uid", uid);
                            intent.putExtra("dept", dept);
                            intent.putExtra("curryear", curryear);
                            intent.putExtra("Subject", s4);
                            intent.putExtra("Activity", Activity);
                            startActivityForResult(intent, 4);
                        } else if (subjects.contains("DDB"))
                        {
                            dialogBuilder
                                    .withTitle("Feedback System")
                                    .withMessage("You have already filled the form.")
                                    .withDialogColor("#1976D2")
                                    .withButton1Text("OK")
                                    .withDuration(700)
                                    .withEffect(Effectstype.RotateBottom)
                                    .setButton1Click(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialogBuilder.cancel();
                                        }
                                    })
                                    .show();
                        }
                           // Toast.makeText(SubjectActivity.this, "You have already filled the form", Toast.LENGTH_SHORT).show();
                    }
                });

                c5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!(subjects.contains("SPCC"))) {

                            Intent intent = new Intent(SubjectActivity.this, FeedbackActivity.class);
                            intent.putExtra("uid", uid);
                            intent.putExtra("dept", dept);
                            intent.putExtra("curryear", curryear);
                            intent.putExtra("Subject", s5);
                            intent.putExtra("Activity", Activity);
                            startActivityForResult(intent, 5);
                        } else if (subjects.contains("SPCC"))
                        {
                            dialogBuilder
                                    .withTitle("Feedback System")
                                    .withMessage("You have already filled the form.")
                                    .withDialogColor("#1976D2")
                                    .withButton1Text("OK")
                                    .withDuration(700)
                                    .withEffect(Effectstype.RotateBottom)
                                    .setButton1Click(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialogBuilder.cancel();
                                        }
                                    })
                                    .show();
                        }
                           // Toast.makeText(SubjectActivity.this, "You have already filled the form", Toast.LENGTH_SHORT).show();
                    }
                });


                c6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!(subjects.contains("Elective"))) {

                            Intent intent = new Intent(SubjectActivity.this, FeedbackActivity.class);
                            intent.putExtra("uid", uid);
                            intent.putExtra("dept", dept);
                            intent.putExtra("curryear", curryear);
                            intent.putExtra("Subject", s6);
                            intent.putExtra("Activity", Activity);
                            startActivityForResult(intent, 6);
                        } else if (subjects.contains("Elective"))
                        {
                            dialogBuilder
                                    .withTitle("Feedback System")
                                    .withMessage("You have already filled the form.")
                                    .withDialogColor("#1976D2")
                                    .withButton1Text("OK")
                                    .withDuration(700)
                                    .withEffect(Effectstype.RotateBottom)
                                    .setButton1Click(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialogBuilder.cancel();
                                        }
                                    })
                                    .show();
                        }
                           // Toast.makeText(SubjectActivity.this, "You have already filled the form", Toast.LENGTH_SHORT).show();
                    }
                });


        }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {

            if (resultCode == RESULT_OK) {

            }
        }
        if (requestCode == 2) {

            if (resultCode == RESULT_OK) {

            }
        }
        if (requestCode == 3) {

            if (resultCode == RESULT_OK) {

            }
        }
        if (requestCode == 4) {

            if (resultCode == RESULT_OK) {

            }
        }
        if (requestCode == 5) {

            if (resultCode == RESULT_OK) {

            }
        }
        if (requestCode == 6) {

            if (resultCode == RESULT_OK) {

            }
        }
    }
}
