package com.feedbacksystem.Students;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.feedbacksystem.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

public class LoginButtonActivity extends AppCompatActivity {
    private EditText uid;
    private EditText password;
    private Button login;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
   // SharedPreferences mPrefs = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
    private String TAG="";
    private String pass;
    private String mail;
    private String name;
    private String UID;


    @Override
    protected void onCreate(Bundle savedinstances) {
        super.onCreate(savedinstances);
        setContentView(R.layout.activity_loginbutton);
        uid = (EditText) findViewById(R.id.loginuid);
        password = (EditText) findViewById(R.id.loginpass);
        final TextInputLayout usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        final TextInputLayout passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
        usernameWrapper.setHint("Username");
        passwordWrapper.setHint("Password");
        login = (Button) findViewById(R.id.login);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();

      /*  if (mPrefs.getBoolean("is_logged_before",true)) {
           Intent i = new Intent(this, DashboardActivity.class);
           startActivity(i);
        } else {
            // continue to login part
        }*/

//System.out.println(FLAG);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                System.out.println(user);
                if (user != null) {
                // count++;
                   // System.out.println(count);
                  //  if(count%2!=0)
                    //{

                        final String email = user.getEmail();
                        System.out.println("SignedIn");
                        Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                        progressDialog.setMessage("Logging in User");
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                        progressDialog.setCanceledOnTouchOutside(false);

                        FirebaseDatabase database = FirebaseDatabase.getInstance();

                        final DatabaseReference myRef = database.getReference("User");

                        //System.out.println(myRef.child(ui).getKey());
                        myRef.orderByChild("email").equalTo(email).limitToFirst(1).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                                    UID =  childSnapshot.getKey();

                                    mail = (String) childSnapshot.child("email").getValue();
                                    pass = (String) childSnapshot.child("password").getValue();
                                    name = (String) childSnapshot.child("name").getValue();
                                }
                               /* System.out.println("Password1 " + pass);
                                System.out.println("Email1 " + email);
                                System.out.println("UID1 " + UID);
                                System.out.println("Finaluid " + UID);*/
                               System.out.println("Loginbutton");
                                progressDialog.cancel();
                                int year = Calendar.getInstance().get(Calendar.YEAR);
                                int curryear = year - Integer.parseInt(UID.substring(0, 4));
                                int depno = Integer.parseInt(UID.substring(5, 6));
                                System.out.println(depno);

                                Intent intent = new Intent(LoginButtonActivity.this, DashboardActivity.class);
                                intent.putExtra("uid", UID);
                                intent.putExtra("name", name);
                                intent.putExtra("curryear", curryear);
                                if (depno == 1)
                                    intent.putExtra("depno", "ETRX");
                                else if (depno == 2)
                                    intent.putExtra("depno", "EXTC");
                                else if (depno == 3)
                                    intent.putExtra("depno", "COMPS");
                                else if (depno == 4)
                                    intent.putExtra("depno", "IT");
                              //  FLAG="1";
                                finish();
                                startActivity(intent);
                            }


                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }

                        });
                    //}
                 /*   myRef.child(UID).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                             email = (String) dataSnapshot.child("email").getValue();
                             pass = (String) dataSnapshot.child("password").getValue();
                            final String name = (String) dataSnapshot.child("name").getValue();
                            System.out.println("Password "+password);
                            System.out.println("Email "+email);
                        }
*/

                            /*String acctname = (String) dataSnapshot.child("name").getRef().getParent().getKey();
                            System.out.println("tffffffffffffffffffffff" + acctname);
                            String mail = (String) dataSnapshot.child("email").getValue();
                            String password = (String) dataSnapshot.child("password").getValue();
                            final String name = (String) dataSnapshot.child("name").getValue();
                            System.out.println(password);
                            System.out.println(email);*/
                            // User is signed in

                           // String email = user.getEmail();
                          //  System.out.println(email);

                }

            else {
                    // User is signed out

                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    System.out.println("SignedOut");
                    // FLAG="0";
                }

            }};

                login.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //  Toast.makeText(LoginButtonActivity.this,"Username:"+uid.getText().toString(),Toast.LENGTH_SHORT).show();
                        //  Toast.makeText(LoginButtonActivity.this,"Password:"+password.getText().toString(),Toast.LENGTH_SHORT).show();
                        // if(FLAG==0)
                        userlogin();
                        //  else
                        //    Toast.makeText(LoginButtonActivity.this,"You are logged in",Toast.LENGTH_SHORT).show();
                    }
                });

            }



  /*  public void onSuccess(Map<String, String> result) {
        String ui=uid.getText().toString();
        System.out.println("Successfully created user account with uid: " + result.get("uid"));
        mPrefs = this.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("userId",ui );
        editor.putBoolean("is_logged_before",true); //this line will do trick
        editor.commit();
    }*/


    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            firebaseAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public void userlogin()
    {
        final String ui = uid.getText().toString().trim();
        final String pass = password.getText().toString().trim();
        System.out.println(pass);
        if(TextUtils.isEmpty(ui))
        {
            Toast.makeText(this,"Please enter your UID",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(pass))
        {
            Toast.makeText(this,"Please enter the Password",Toast.LENGTH_SHORT).show();
            return;
        }


        progressDialog.setMessage("Logging in User");
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

            FirebaseDatabase database = FirebaseDatabase.getInstance();

            DatabaseReference myRef = database.getReference("User");

      //  System.out.println(myRef.child(ui).getKey());
        myRef.child(ui).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String acctname = dataSnapshot.child("name").getRef().getParent().getKey();
               // System.out.println("tffffffffffffffffffffff"+acctname);
                String email = (String) dataSnapshot.child("email").getValue();
                String password=(String)dataSnapshot.child("password").getValue();
               final String name=(String)dataSnapshot.child("name").getValue();
              //  System.out.println(password);
                //System.out.println(email);
                if(email!=null && password!=null) {
                    firebaseAuth.signInWithEmailAndPassword(email, pass)
                            .addOnCompleteListener(LoginButtonActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    progressDialog.cancel();
                                    if (task.isSuccessful()) {
                                        Toast.makeText(LoginButtonActivity.this,"Login Successful",Toast.LENGTH_SHORT).show();
                                    }
                                    if (!task.isSuccessful()) {
                                        // there was an error
                                        progressDialog.cancel();
                                        if (pass.length() < 6) {
                                            Toast.makeText(LoginButtonActivity.this, "Please enter password more than 6 characters", Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(LoginButtonActivity.this, "Login Failed", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            });
                }
                else {
                    progressDialog.cancel();

                    Toast.makeText(LoginButtonActivity.this, "Login Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Cancelled");
                progressDialog.cancel();
                Toast.makeText(LoginButtonActivity.this,"Unable to Login",Toast.LENGTH_SHORT).show();
            }
        });


    }
}
