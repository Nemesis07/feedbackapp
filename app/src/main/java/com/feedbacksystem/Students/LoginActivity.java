package com.feedbacksystem.Students;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.feedbacksystem.R;

public class LoginActivity extends AppCompatActivity {
    private Button barlogin;
    private Button login;
    @Override
    protected void onCreate(Bundle savedinstances){
    super.onCreate(savedinstances);
    setContentView(R.layout.activity_login);

    barlogin=(Button)findViewById(R.id.barlogin);
    login=(Button)findViewById(R.id.login);


    barlogin.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
            LoginActivity.this.startActivity(intent);

        }
    });
    login.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this,LoginButtonActivity.class);
            LoginActivity.this.startActivity(intent);

        }
    });
}
    @Override
    public void onBackPressed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(true)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        LoginActivity.this.finish();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();

    }
}
